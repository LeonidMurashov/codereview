import pygame
import pygame.locals
from pygame.locals import Rect
import pygame.font

from pygments.lexers.c_cpp import CppLexer
from pygments.styles.vs import VisualStudioStyle

import random
import collections
import time

from config import *


class Word:
    to_destroy = False
    physics_on = False

    def __init__(self, text, x, y, color, screen, font, screen_height):
        self.text = text
        self.color = color
        self.vx = 0
        self.vy = 0
        self.x = x
        self.y = y
        self.rotation_v = 0
        self.angle = 0
        self.screen_height = screen_height
        self.screen = screen
        self.text_surface = font.render(self.text, True, self.color)

    def step(self, delta_t, speed_for_text):
        self.x += self.vx * delta_t
        if self.physics_on:
            self.y += self.vy * delta_t
        else:
            self.y += speed_for_text * delta_t

        self.angle += self.rotation_v * delta_t
        if self.physics_on:
            self.vy += ACCELERATION * delta_t

        if self.y > self.screen_height:
            self.to_destroy = True

    def blow(self):
        self.enable_physics()
        self.vy = - 300 - 300 * random.random()
        self.vx = 400 * (0.5 - random.random())
        self.rotation_v = 400 * (0.5 - random.random())

    def render(self):
        if self.y > 0:
            pygame.transform.rotate(self.text_surface, 100)
            self.screen.blit(self.text_surface, (self.x, self.y))

    def enable_physics(self):
        self.physics_on = True


def get_words_coloring(row):
    coloring = []
    for token, value in CppLexer().get_tokens(row):
        colour = VisualStudioStyle.style_for_token(token)['color']
        if colour:
            r, g, b = int(colour[0:2], 16), int(colour[2:4], 16), int(colour[4:6], 16)
        else:
            r = g = b = 255
        coloring.append([value, (r, g, b)])
    return coloring


def generate_text(screen, text_x, text_y, font, screen_height):
    while True:
        with open("main.cpp") as f:
            t = f.read()

        rows = reversed(t.split('\n'))

        y = text_y
        for row in rows:
            x = text_x
            words_row = []
            for word, color in get_words_coloring(row):
                words_row.append(Word(word.rstrip(), x, y, color, screen, font, screen_height))
                x += font.size(word)[0]
            if len(words_row) > 0:
                yield words_row


class Game:
    def __init__(self, screen: pygame.Surface):
        font = pygame.font.Font(r"consola.ttf", CODE_SIZE)
        self.font_height = font.get_height()

        self.text_iterator = iter(generate_text(screen, CODE_OFFSET[0], 0, font, screen.get_height()))
        self.text = collections.deque()
        self.all_words = []
        self.lines_reviewed = 0
        self.lines_lost = 0
        self.initial_speed = INITIAL_START_SPEED

    def click(self):
        # Blowing up WORDS_PER_CLICK
        for i in range(WORDS_PER_CLICK):

            # Check for no words on the screen
            if len(self.text) > 0:
                row = self.text[-1]
                row[-1].blow()
                self.text[-1].pop(len(self.text[-1]) - 1)

                # Check if the word was the last in the line
                if len(self.text[-1]) == 0:
                    self.text.pop()
                    self.lines_reviewed += 1

    def step(self, elapsed_time):
        # Add line from generator
        if len(self.text) == 0 or min(row[0].y for row in self.text) >= self.font_height:
            row = next(self.text_iterator)
            self.all_words += row
            self.text.insert(0, row)

        # Step all words
        indices_to_delete = []
        for i, word in enumerate(self.all_words):
            word.step(elapsed_time, self.initial_speed)

        # If last non-blown line is to_destroy, it is lost
        while self.text[-1][0].to_destroy and not self.text[-1][0].physics_on:
            self.lines_lost += 1
            self.text.pop()

        # Delete to_destroy words
        for i, word in enumerate(self.all_words):
            if word.to_destroy:
                indices_to_delete.append(i)
        for i in reversed(indices_to_delete):
            del self.all_words[i]

        # Adjust speed to keep the last line on equilibrium
        if len(self.text) > EQUILIBRIUM_LINES_NUMBER and self.initial_speed > MIN_CODE_SPEED:
            self.initial_speed -= (len(self.text) - EQUILIBRIUM_LINES_NUMBER) * DAMPING_RATE
        elif len(self.text) <= EQUILIBRIUM_LINES_NUMBER and self.initial_speed < MAX_CODE_SPEED:
            self.initial_speed -= len(self.text) - EQUILIBRIUM_LINES_NUMBER

    def render(self):
        for word in self.all_words:
            word.render()

    def get_lines_reviewed(self):
        return self.lines_reviewed

    def get_lines_lost(self):
        return self.lines_lost


def init():
    pygame.font.init()
    pygame.init()


def process_events(game):
    for event in pygame.event.get():
        if event.type == pygame.locals.QUIT or \
                (event.type == pygame.locals.KEYDOWN and
                 event.key == pygame.locals.K_ESCAPE):
            return False
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1 or \
                event.type == pygame.locals.KEYDOWN and event.key == pygame.locals.K_SPACE:
            game.click()
    return True


def main():
    init()

    info_object = pygame.display.Info()
    screen_rect = Rect(0, 0, info_object.current_w / WINDOW_SCREEN_RATIO, info_object.current_h / WINDOW_SCREEN_RATIO)
    screen: pygame.Surface = pygame.display.set_mode(screen_rect.size)
    pygame.display.set_caption("CodeReview!")

    comic_sans = pygame.font.SysFont('Comic Sans MS', TEXT_SIZE)
    clock = pygame.time.Clock()
    game = Game(screen)

    # Game loop
    while True:
        elapsed_time = clock.tick_busy_loop() / 1000

        rc = process_events(game)
        if not rc:
            return

        game.step(elapsed_time)
        game.render()

        score = comic_sans.render('Lines reviewed: {}'.format(game.get_lines_reviewed()), True, YELLOW)
        screen.blit(score,
                    (screen.get_width() + REVIEWED_TEXT_OFFSET[0],
                     screen.get_height() + REVIEWED_TEXT_OFFSET[1]))

        score = comic_sans.render('Lines lost: {}'.format(game.get_lines_lost()), True, RED)
        screen.blit(score,
                    (screen.get_width() + LOST_TEXT_OFFSET[0],
                     screen.get_height() + LOST_TEXT_OFFSET[1]))

        pygame.display.flip()
        time.sleep(0.001)
        screen.fill(GREY)


if __name__ == "__main__":
    import sys

    sys.exit(main())
